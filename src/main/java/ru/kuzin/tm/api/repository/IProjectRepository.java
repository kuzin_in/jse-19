package ru.kuzin.tm.api.repository;

import ru.kuzin.tm.model.Project;

public interface IProjectRepository extends IRepository<Project> {

    Project create(String name, String description);

    Project create(String name);

}