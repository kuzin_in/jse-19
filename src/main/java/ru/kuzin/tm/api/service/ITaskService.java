package ru.kuzin.tm.api.service;

import ru.kuzin.tm.enumerated.Status;
import ru.kuzin.tm.model.Task;

import java.util.List;

public interface ITaskService extends IService<Task> {

    Task updateById(String id, String name, String description);

    Task updateByIndex(Integer index, String name, String description);

    Task changeTaskStatusById(String id, Status status);

    Task changeTaskStatusByIndex(Integer index, Status status);

    List<Task> findAllByProjectId(String projectId);

    Task create(String name, String description);

    Task create(String name);

}