package ru.kuzin.tm.api.service;

import ru.kuzin.tm.enumerated.Status;
import ru.kuzin.tm.model.Project;

public interface IProjectService extends IService<Project> {

    Project updateById(String id, String name, String description);

    Project updateByIndex(Integer index, String name, String description);

    Project changeProjectStatusById(String id, Status status);

    Project changeProjectStatusByIndex(Integer index, Status status);

    Project create(String name, String description);

    Project create(String name);

}